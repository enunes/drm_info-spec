Name: drm_info
Version: v2.4.0
Release: 1%{?dist}
Summary: Small utility to dump info about DRM devices.
License: MIT
URL: https://gitlab.freedesktop.org/emersion/drm_info
Source0: https://gitlab.freedesktop.org/emersion/%{name}/-/archive/%{version}/%{name}-%{version}.tar.gz

BuildRequires: gcc json-c-devel meson ninja-build pciutils-devel scdoc
BuildRequires: libdrm-devel >= 2.4.114

%description
drm_info is a small utility to dump info about DRM devices.

%prep
%setup -q

%build
%meson
%meson_build

%install
%meson_install

%files
%{_bindir}/drm_info
%{_mandir}/man1/drm_info.1.gz

%changelog
* Tue Nov 22 2022 Erico Nunes <nunes.erico@gmail.com> v2.4.0-1
- Update to v2.4.0
- Update to new URL

* Mon Jan 31 2022 Erico Nunes <nunes.erico@gmail.com> 2.3.0-1
- Initial package
